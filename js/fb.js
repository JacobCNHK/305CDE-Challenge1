var database;
var currId;
var currK;
var users = [];
var sortType = '';
var sortReverse = false;

function setup() {
    document.getElementById("btnUpdate").style.display = "none";
    document.getElementById("btnCancel").style.display = "none";
    document.getElementById("btnAdd").style.display = "block";
    document.getElementById('users_list').style.display = 'block';
    document.getElementById('register-form').style.display = 'none';
    var config = {
        apiKey: "AIzaSyCcr3dyFScB_pIYFuOug-5r3IfGXl0YaG4",
        authDomain: "challenge1-user-registration.firebaseapp.com",
        databaseURL: "https://challenge1-user-registration.firebaseio.com",
        projectId: "challenge1-user-registration",
        storageBucket: "challenge1-user-registration.appspot.com",
        messagingSenderId: "643624137209"
    };
    firebase.initializeApp(config);
    database = firebase.database();
    
    var ref = database.ref('users');
    ref.on('value', getData, errData);
}

document.querySelector('#users-list').onclick = function() {
    document.getElementById('users_list').style.display = 'block';
    document.getElementById('register-form').style.display = 'none';
}

document.querySelector('#register').onclick = function() {
    document.getElementById('users_list').style.display = 'none';
    document.getElementById('register-form').style.display = 'block';
}

document.querySelector('#username').onchange = function() {
    var username = document.querySelector('#username').value;
    for (var i=0; i<users.length; i++) {
        if(users[i].username == username) {
            alert('The username is repeated!');
        }
    }
}

document.querySelector('#pwd').onchange = function() {
    var pwd = document.querySelector('#pwd').value;
    if (pwd.length < 8) {
        alert('The password need more than or equal 8 words!');
    }
}

document.querySelector('#cfm_pwd').onchange = function() {
    var pwd = document.querySelector('#pwd').value;
    var cfm_pwd = document.querySelector('#cfm_pwd').value;
    if (pwd != cfm_pwd) {
        alert('Both of password is not same!');
    }
}

document.querySelector('#email').onchange = function() {
    var email = document.querySelector('#email').value;
    if (!(isEmailAddress(email))) {
        alert('The email address is not validation! For example: abc@abc.com'); 
    }
}

document.querySelector('#name').onchange = function() {
    var name = document.querySelector('#name').value;
    if(!(isName(name))) {
        alert('The name is not validation!');
    }
}

function isEmailAddress(str) {
    var pattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return pattern.test(str);
}

function isName(str) {
    var pattern = /^[A-Za-z\s]+$/;
    return pattern.test(str);
}

document.querySelector('#age').onchange = function() {
    var age = document.querySelector('#age').value;
    if (parseInt(age) < 1 || parseInt(age) > 100) {
        alert('The range of age: 1 to 100');
    }
}

document.querySelector('#filterType').onchange = function() {
	var filterType = document.querySelector('#filterType').value;
    var filterValue = parseInt(document.querySelector('#tf_filter_age').value);
    if (!(filterValue >= 0)) {
        filterValue = -1;
    }
    var table, tr, td, i;
    table = document.getElementById("tbody");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = parseInt(tr[i].getElementsByTagName("td")[4].innerHTML);
        if (td) {
            if (filterType == ">"  || filterValue == -1) {
                if (td >= filterValue) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            } else {
                if (td <= filterValue) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }       
    }
}

document.querySelector('#tf_filter_age').onkeyup = function() {
    var filterType = document.querySelector('#filterType').value;
    var filterValue = parseInt(document.querySelector('#tf_filter_age').value);
    if (!(filterValue >= 0)) {
        filterValue = -1;
    }
    var table, tr, td, i;
    table = document.getElementById("tbody");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = parseInt(tr[i].getElementsByTagName("td")[4].innerHTML);
        if (td) {
            if (filterType == ">") {
                if (td >= filterValue) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            } else {
                console.log("<")
                if (td <= filterValue || filterValue == -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }       
    }
}

document.querySelector('#btnAdd').onclick = function() {
    console.log('add user');
	addUser();
}

document.querySelector('#avg_age').onclick = function() {
    var sum = 0;
    for (var i = 0; i < users.length; i++) {
        sum += parseInt(users[i].age);
    }
    var avg = sum / users.length;
	if (users.length == 0) {
		avg = 0;
	}
    alert('Average Age: ' + avg);
}

document.querySelector('#btnUpdate').onclick = function() {
    var username = document.querySelector('#username').value;
    var name = document.querySelector('#name').value;
    var email = document.querySelector('#email').value;
    var pwd = document.querySelector('#pwd').value;
    var age = document.querySelector('#age').value;
    console.log(pwd);
    var data = {
        username: username,
        name: name,
        email: email,
        password: pwd,
        age: age
    };
    users[currK] = {
        id: currId,
        username: username,
        name: name,
        email: email,
        password: pwd,
        age: age
    };
    var ref = database.ref('users');
    ref.child(currId).set(data);
    setup();
    reset();
    document.getElementById("btnUpdate").style.display = "none";
    document.getElementById("btnAdd").style.display = "block";
}

function clear() {
	var tbody = document.getElementById('tbody');
	tbody.innerHTML = "";
}

function cancel() {
    document.getElementById("btnUpdate").style.display = "none";
    document.getElementById("btnCancel").style.display = "none";
    document.getElementById("btnAdd").style.display = "block";
}

function reset() {
    document.querySelector("#username").value = "";
    document.querySelector("#name").value = "";
    document.querySelector("#email").value = "";
    document.querySelector("#pwd").value = "";
    document.querySelector("#age").value = "";
}

function updateUser(element) {
	currId = element.parentNode.parentNode.id;
	currK = element.parentNode.parentNode.k;
    document.getElementById("btnUpdate").style.display = "block";
    document.getElementById("btnCancel").style.display = "block";
    document.getElementById("btnAdd").style.display = "none";
    document.querySelector("#username").value = users[currK].username;
    document.querySelector("#name").value = users[currK].name;
    document.querySelector("#email").value = users[currK].email;
    document.querySelector("#pwd").value = users[currK].password;
	document.querySelector("#cfm_pwd").value = users[currK].password;
    document.querySelector("#age").value = users[currK].age;
}

function removeUser(element) {
	var id = element.parentNode.parentNode.id;
	users.splice(id, 1);
    var ref = database.ref('users');
    ref.child(id).remove();
}

function addUser() {
	console.log('add user');
    var username = document.querySelector('#username').value;
    var name = document.querySelector('#name').value;
    var email = document.querySelector('#email').value;
    var pwd = document.querySelector('#pwd').value;
    var cfm_pwd = document.querySelector('#cfm_pwd').value;
    var age = document.querySelector('#age').value;
    if (username.length > 0 && name.length > 0 && email.length > null && pwd.length > 0 && cfm_pwd.length > 0 && age.length > 0) {
        var data = {
            username: username,
            name: name,
            email: email,
            password: pwd,
            age: age
        }
        var ref = database.ref('users/');
        ref.push(data); 
    } else {
        alert('The form has not been completed!');
    }
}

function loadList() {
    console.log('load list');
    clear();
	var tbody = document.getElementById('tbody');
	for (var i=0; i<users.length; i++) {
		var row = document.createElement('tr');
		row.id = users[i].id;
		row.k = i;
		row.innerHTML = '<td>'+users[i].username+'</td><td>'+users[i].name+'</td><td>'+users[i].email+'</td><td>'+users[i].password+'</td><td>'+users[i].age+'</td><td><a onclick="updateUser(this)" href="#">Update</a></td><td><a onclick="removeUser(this)" href="#">Delete</a></td>';
		tbody.appendChild(row);
	}
}

function getData(data) {
    var results = data.val();
    var keys = Object.keys(results);
    for (var i = 0; i < keys.length; i++) {
        var k = keys[i];
        var username = results[k].username;
        var name = results[k].name;
        var email = results[k].email;
        var pwd = results[k].password;
        var age = results[k].age;
        
        users[i] = {
            id: k,
            username: username,
            name: name,
            email: email,
            password: pwd,
            age: age
        };
    }
    loadList();
}

function errData(err) {
    console.log('Error! ' + err);
}

// sorting
var TableIDvalue = "list";
var TableLastSortedColumn = -1;

function SortTable() {
    var sortColumn = parseInt(arguments[0]);
    var type = arguments.length > 1 ? arguments[1] : 'T';
    var table = document.getElementById(TableIDvalue);
    var tbody = table.getElementsByTagName("tbody")[0];
    var rows = tbody.getElementsByTagName("tr");
    var arrayOfRows = new Array();
    type = type.toUpperCase();
	
    for (var i = 0, len = rows.length; i < len; i++) {
        arrayOfRows[i] = new Object;
        arrayOfRows[i].oldIndex = i;
        var celltext = rows[i].getElementsByTagName("td")[sortColumn].innerHTML.replace(/<[^>]*>/g, "");
        if (type != 'D') {
            var re = type == "N" ? /[^\.\-\+\d]/g : /[^a-zA-Z0-9]/g;
            arrayOfRows[i].value = celltext.replace(re, "").substr(0, 25).toLowerCase();
        }
    }
    if (sortColumn == TableLastSortedColumn) {
        arrayOfRows.reverse();
    } else {
        TableLastSortedColumn = sortColumn;
        switch (type) {
            case "N" :
                arrayOfRows.sort(CompareRowOfNumbers);
                break;
            default  :
                arrayOfRows.sort(CompareRowOfText);
        }
    }
    var newTableBody = document.createElement("tbody");
    for (var i = 0, len = arrayOfRows.length; i < len; i++) {
        newTableBody.appendChild(rows[arrayOfRows[i].oldIndex].cloneNode(true));
    }
    table.replaceChild(newTableBody, tbody);
} // function SortTable()

function CompareRowOfText(a, b) {
    var aval = a.value;
    var bval = b.value;
    return(aval == bval ? 0 : (aval > bval ? 1 : -1));
} // function CompareRowOfText()

function CompareRowOfNumbers(a, b) {
    var aval = /\d/.test(a.value) ? parseFloat(a.value) : 0;
    var bval = /\d/.test(b.value) ? parseFloat(b.value) : 0;
    return(aval == bval ? 0 : (aval > bval ? 1 : -1));
} // function CompareRowOfNumbers()